const { networkInterfaces } = require('os');
const express = require('express');
// const { ML } = require('./utils');

const app = express();
const api = require('./config');

require('./loaders')(app);

const nets = networkInterfaces();
const results = {};

const PORT = api.port;

app.listen(PORT, err => {
  if (err) {
    console.log(err);
    return process.exit(1);
  }

  Object.keys(nets).forEach(name => {
    nets[name].forEach(net => {
      if (net.family === 'IPv4' && !net.internal) {
        if (!results[name]) {
          results[name] = [];
        }
        results[name].push(net.address);
      }
    });
  });
  console.log(`Server is running on ${results[Object.keys(results)[0]]}:${PORT}`);
});
