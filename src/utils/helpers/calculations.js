const { default: distance } = require('@turf/distance');

const calculateElevation = coords =>
  coords.reduce((acc, point, i, array) => {
    try {
      const from = point[2];
      const to = array[i + 1][2];

      const elevationDifference = from - to;
      if (elevationDifference > 0) {
        return acc + elevationDifference;
      }
      return acc;
    } catch (err) {
      return acc;
    }
  }, 0);

const calculateDistance = coords =>
  coords.reduce((acc, point, i, array) => {
    try {
      const from = point;
      const to = array[i + 1];

      const currentPointDistance = distance(from, to);
      return acc + currentPointDistance;
    } catch (err) {
      return acc;
    }
  }, 0);

const calculateMaxMinSpeed = coords => {
  let minSpeed = coords[0];
  let maxSpeed = coords[0];
  coords.forEach((point, i, array) => {
    try {
      const from = point;
      const to = array[i + 1];

      const currentPointDistance = distance(from, to);
      if (minSpeed > currentPointDistance) {
        minSpeed = currentPointDistance;
      }
      if (maxSpeed < currentPointDistance) {
        maxSpeed = currentPointDistance;
      }
    } catch (err) {}
  }, 0);

  return { maxSpeed, minSpeed };
};

module.exports = {
  calculateDistance,
  calculateElevation,
  calculateMaxMinSpeed,
};
