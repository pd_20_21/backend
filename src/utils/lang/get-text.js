const { get } = require('lodash');
const en = require('./en.json');
const tr = require('./tr.json');
const pt = require('./pt.json');

const dictionary = {
  en,
  tr,
  pt,
};

module.exports = (lang, key) => get(dictionary, [lang][key]);
