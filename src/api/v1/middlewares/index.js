const imageUpload = require('./image-upload');
const gpxUpload = require('./gpx-upload');
const bulkGpxUpload = require('./bulk-gpx-upload');
const objectIdControl = require('./object-id-control');

module.exports = {
  imageUpload,
  gpxUpload,
  bulkGpxUpload,
  objectIdControl,
};
