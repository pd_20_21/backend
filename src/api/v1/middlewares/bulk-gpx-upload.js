const multer = require('multer');
const { extname } = require('path');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'tmp/');
  },
  filename: (req, file, cb) => {
    const ext = extname(file.originalname);
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    cb(null, `${file.fieldname}-${uniqueSuffix}${ext}`);
  },
});

const allowedFiles = (req, file, cb) => {
  // Accept images only
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
    req.fileValidationError = 'Only jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF file type are allowed!';
    return cb(new Error('Only jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF file type  are allowed!'), false);
  }
  return cb(null, true);
};

module.exports = multer({
  storage,
  limits: { fileSize: 100000000 },
  allowedFiles,
}).array('gpx', 10);
