const multer = require('multer');
const { extname } = require('path');

const storage = multer.diskStorage({
  destination: './tmp',
  filename: (req, file, cb) => {
    const ext = extname(file.originalname);
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    cb(null, `${file.fieldname}-${uniqueSuffix}${ext}`);
  },
});
const fileFilter = (_req, file, cb) => cb(null, true);

module.exports = multer({
  storage,
  limits: { fileSize: 1000000 },
  fileFilter,
}).single('gpx');
