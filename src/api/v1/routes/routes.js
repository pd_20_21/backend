const express = require('express');

const router = express.Router();

const routeController = require('../controllers/route');
// const routeValidator = require('../validators/route.validator');

// LIST
router.get('/', routeController.listRoutes);
router.get('/:routeId', routeController.getRoute);

// CREATE
router.post('/', routeController.createRoute);
router.patch('/:routeId', routeController.patchRoute);

module.exports = router;
