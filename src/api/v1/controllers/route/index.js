// LIST
const listRoutes = require('./listRoutes');
const getRoute = require('./getRoute');

// CREATE
const createRoute = require('./createRoute');
const createRouteBulk = require('./createRouteBulk');
const patchRoute = require('./patchRoute');

module.exports = {
  listRoutes,
  getRoute,
  createRoute,
  createRouteBulk,
  patchRoute,
};
