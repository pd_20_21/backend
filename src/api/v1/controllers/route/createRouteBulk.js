const { readFileSync } = require('fs');
const tj = require('@mapbox/togeojson');
const { DOMParser } = require('xmldom');

module.exports = async (req, res) => {
  const { files } = req;
  console.log('file: createRouteBulk.js ~ line 8 ~ module.exports= ~ files', files);
  const geoJsonFiles = files.map(file => {
    const realfile = new DOMParser().parseFromString(readFileSync(`./${file.path}`, 'utf-8'));
    const geoJson = tj.gpx(realfile);
    console.log('file: createRouteBulk.js ~ line 12 ~ module.exports= ~ geoJson', geoJson.features);
    return { originalname: file.originalname, geoJson };
  });

  return res.status(200).json({
    // geojsonFromGpx,
  });
};

/**
 * @swagger
 * /v1/route:
 *    put:
 *      summary: Transform the gpx into a geojson and create route
 *      parameters:
 *        - in: header
 *          name: Authorization
 *          schema:
 *            type: string
 *          description: Put access token here
 *      tags:
 *        - Route
 *      responses:
 *        "200":
 *          description: The route information has gotten successfully.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          resultMessage:
 *                              $ref: '#/components/schemas/ResultMessage'
 *                          resultCode:
 *                              $ref: '#/components/schemas/ResultCode'
 *                          user:
 *                              $ref: '#/components/schemas/User'
 *        "401":
 *          description: Invalid token.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 *        "500":
 *          description: An internal server error occurred, please try again.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 */
