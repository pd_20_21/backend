const { get, isNil } = require('lodash');
const { pool } = require('../../../../loaders/database');

module.exports = async (req, res) => {
  console.log('file: listRoutes.js ~ line 9 ~ module.exports= ~ req.query', req.query);
  const client = pool();
  let { activity, difficulty, maxDuration, minDuration, minDistance, maxDistance } = req.query;
  if (!isNil(activity)) {
    if (Array.isArray(activity)) {
      activity = activity.map(act => parseInt(act, 10));
    } else {
      activity = [parseInt(activity, 10)];
    }
  }
  if (!isNil(difficulty)) {
    if (Array.isArray(difficulty)) {
      difficulty = difficulty.map(act => parseInt(act, 10));
    } else {
      difficulty = [parseInt(difficulty, 10)];
    }
  }
  if (maxDuration) {
    maxDuration = parseInt(maxDuration, 10);
  }
  if (minDuration) {
    minDuration = parseInt(minDuration, 10);
  }
  if (maxDistance) {
    maxDistance = parseInt(maxDistance, 10);
  }
  if (minDistance) {
    minDistance = parseInt(minDistance, 10);
  }
  let cmd = 'SELECT id, name, difficulty, activityType, distance FROM wrr_routes WHERE show=true';
  let countCmd = 'SELECT count(*) FROM wrr_routes WHERE show=true';
  let newClause;
  if (activity) {
    newClause = ` AND activityType IN (${activity.join(', ')})`;
    cmd += newClause;
    countCmd += newClause;
  }
  if (difficulty) {
    newClause = ` AND difficulty IN (${difficulty.join(', ')})`;
    cmd += newClause;
    countCmd += newClause;
  }
  if (!isNil(maxDistance) && !isNil(minDistance)) {
    newClause = ` AND distance BETWEEN ${minDistance} AND ${maxDistance}`;
    cmd += newClause;
    countCmd += newClause;
  } else if (!isNil(maxDistance)) {
    newClause = ` AND distance < ${maxDistance}`;
    cmd += newClause;
    countCmd += newClause;
  } else if (!isNil(minDistance)) {
    newClause = ` AND distance > ${minDistance}`;
    cmd += newClause;
    countCmd += newClause;
  }
  if (!isNil(maxDuration) && !isNil(minDuration)) {
    newClause = ` AND duration BETWEEN ${minDuration} AND ${maxDuration}`;
    cmd += newClause;
    countCmd += newClause;
  } else if (!isNil(maxDuration)) {
    newClause = ` AND duration < ${maxDuration}`;
    cmd += newClause;
    countCmd += newClause;
  } else if (!isNil(minDuration)) {
    newClause = ` AND duration > ${minDuration}`;
    cmd += newClause;
    countCmd += newClause;
  }
  await client.connect();

  console.log('file: listRoutes.js ~ line 82 ~ module.exports= ~ cmd', cmd);
  console.log('file: listRoutes.js ~ line 83 ~ module.exports= ~ countCmd', countCmd);
  const results = await client.query(cmd);
  const count = await client.query(countCmd);
  await client.end();
  const items = get(results, 'rows');
  const totalCount = parseInt(get(count, ['rows', 0, 'count']), 10);

  return res.status(200).json({
    count: items.length,
    totalCount,
    results: items,
  });
};

/**
 * @swagger
 * /route:
 *    get:
 *      summary: Get User Info
 *      parameters:
 *        - in: header
 *          name: Authorization
 *          schema:
 *            type: string
 *          description: Put access token here
 *      tags:
 *        - User
 *      responses:
 *        "200":
 *          description: The user information has gotten successfully.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          resultMessage:
 *                              $ref: '#/components/schemas/ResultMessage'
 *                          resultCode:
 *                              $ref: '#/components/schemas/ResultCode'
 *                          user:
 *                              $ref: '#/components/schemas/User'
 *        "401":
 *          description: Invalid token.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 *        "500":
 *          description: An internal server error occurred, please try again.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 */
