const { readFileSync, existsSync } = require('fs');
const { join } = require('path');
const moment = require('moment');
const { get } = require('lodash');
const { pool } = require('../../../../loaders/database');
const {
  calculations: { calculateDistance, calculateElevation, calculateMaxMinSpeed },
} = require('../../../../utils');
const { RandomForestClassifier } = require('../../../../utils/ml');

const modelDifficultyPath = join(__dirname, '../../../../models/mlDificultyClassifier.json');
const modelActivityPath = join(__dirname, '../../../../models/mlActivityClassifier.json');

const modelExists = modelPath => existsSync(modelPath);

module.exports = async (req, res) => {
  try {
    const client = pool();
    let geoJson;
    try {
      geoJson = JSON.parse(JSON.stringify(req.body));
    } catch (err) {
      console.log('file: createRoute.js ~ line 27 ~ module.exports= ~ err', err);
      return res.status(500).json({ error: 'failed to parse body into json' });
    }

    const eTime = get(geoJson, ['features', 0, 'properties', 'coordTimes'], []);
    const startTime = moment(eTime[0]);
    const endTime = moment(eTime[eTime.length - 1]);
    const lineString = get(geoJson, ['features', 0, 'geometry', 'coordinates'], []);
    const sportDistance = calculateDistance(lineString);
    const elevation = calculateElevation(lineString);
    const duration = endTime.diff(startTime, 'hour', true);
    const { maxSpeed, minSpeed } = calculateMaxMinSpeed(lineString);

    const speed = sportDistance / duration;

    let modelDifficulty;
    let modelActivity;
    if (modelExists(modelDifficultyPath)) {
      const jsonModel = JSON.parse(readFileSync(modelDifficultyPath));
      modelDifficulty = RandomForestClassifier.load(jsonModel);
    }
    if (modelExists(modelActivityPath)) {
      const jsonModel = JSON.parse(readFileSync(modelActivityPath));
      modelActivity = RandomForestClassifier.load(jsonModel);
    }

    const resultActivity = modelActivity.predict([
      [sportDistance, duration, speed, elevation, maxSpeed, minSpeed],
    ]);
    const resultDifficulty = modelDifficulty.predict([
      [sportDistance, duration, speed, elevation, maxSpeed, minSpeed, resultActivity[0]],
    ]);

    let endingName = 'durante a tarde';
    if (startTime.hours() <= 5) {
      endingName = 'de noite';
    } else if (startTime.hours() <= 7) {
      endingName = 'de madrugada';
    } else if (startTime.hours() <= 12) {
      endingName = 'durante a manhã';
    } else if (startTime.hours() <= 18) {
      endingName = 'durante a tarde';
    } else if (startTime.hours() <= 20) {
      endingName = 'ao final da tarde';
    } else {
      endingName = 'durante a noite';
    }

    let beginningName = 'Volta de Bicicleta';

    switch (resultActivity[0]) {
      case 1:
        beginningName = 'Caminhada';
        break;
      case 2:
        beginningName = 'Corrida';
        break;
      case 3:
        beginningName = 'Volta de Bicicleta';
        break;
      default:
        break;
    }

    const name = `${beginningName} ${endingName}`;

    const cmd = `INSERT INTO \
  wrr_routes (name, distance, difficulty, activityType, startDate, endDate, geojson, activity)\
  VALUES \
  ('${name}',${sportDistance}, ${resultDifficulty[0]},${resultActivity[0]}, '${startTime
      .toISOString()
      .replace(/T/g, ' ')}', \
   '${endTime.toISOString().replace(/T/g, ' ')}', \
   '${JSON.stringify(geoJson).replace(/'/g, '"')}', \
   '${JSON.stringify(geoJson.features[0].geometry)}') RETURNING id;`;

    await client.connect();
    const result = await client.query(cmd);
    await client.end();
    const responseObj = {
      id: get(result, ['rows', 0, 'id']),
      name,
      activityType: resultActivity[0],
      difficulty: resultDifficulty[0],
    };
    console.log('file: createRoute.js ~ line 74 ~ module.exports= ~ responseObj', responseObj);

    return res.status(200).json(responseObj);
  } catch (err) {
    console.log('file: createRoute.js ~ line 71 ~ module.exports= ~ err', err);
    return res.status(500).json({ err });
  }
};

/**
 * @swagger
 * /v1/route:
 *    post:
 *      summary: Transform the gpx into a geojson and create route
 *      parameters:
 *      tags:
 *        - Route
 *      responses:
 *        "200":
 *          description: The route information has gotten successfully.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          resultMessage:
 *                              $ref: '#/components/schemas/ResultMessage'
 *                          resultCode:
 *                              $ref: '#/components/schemas/ResultCode'
 *                          user:
 *                              $ref: '#/components/schemas/User'
 *        "401":
 *          description: Invalid token.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 *        "500":
 *          description: An internal server error occurred, please try again.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 */
