const { get } = require('lodash');
const { pool } = require('../../../../loaders/database');

module.exports = async (req, res) => {
  const client = pool();
  const { routeId } = req.params;
  await client.connect();
  const results = await client.query(`SELECT id, geojson FROM wrr_routes WHERE id=${routeId}`);
  await client.end();
  const items = get(results, 'rows');

  return res.status(200).json({
    result: items.map(item => item.geojson)[0],
  });
};

/**
 * @swagger
 * /v1/route:
 *    get:
 *      summary: Get Route Info
 *      tags:
 *        - Route
 *      responses:
 *        "200":
 *          description: The route information has gotten successfully.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          resultMessage:
 *                              $ref: '#/components/schemas/ResultMessage'
 *                          results:
 *                              type: array
 *        "401":
 *          description: Invalid token.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 *        "500":
 *          description: An internal server error occurred, please try again.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 */
