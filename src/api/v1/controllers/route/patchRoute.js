const { existsSync, writeFileSync, readFileSync } = require('fs');
const { join } = require('path');
const { get } = require('lodash');
const moment = require('moment');
const { pool } = require('../../../../loaders/database');
const {
  calculations: { calculateDistance, calculateElevation, calculateMaxMinSpeed },
} = require('../../../../utils');
const { RandomForestClassifier } = require('../../../../utils/ml');

const modelDifficultyPath = join(__dirname, '../models/mlDificultyClassifier.json');
const modelActivityPath = join(__dirname, '../models/mlActivityClassifier.json');

const modelExists = modelPath => existsSync(modelPath);

module.exports = async (req, res) => {
  try {
    const client = pool();
    const { routeId } = req.params;
    const { name, difficulty, activityType } = req.body;

    const cmd = `UPDATE wrr_routes \
    SET show=true,  name='${name}', difficulty=${parseInt(difficulty, 10)}, activityType=${parseInt(
      activityType,
      10,
    )} \
    WHERE id=${routeId} RETURNING geojson;`;

    // Online learning

    await client.connect();
    const result = await client.query(cmd);
    await client.end();
    const geoJson = get(result, ['rows', 0, 'geojson']);

    const eTime = get(geoJson, ['features', 0, 'properties', 'coordTimes'], []);
    const startTime = moment(eTime[0]);
    const endTime = moment(eTime[eTime.length - 1]);
    const lineString = get(geoJson, ['features', 0, 'geometry', 'coordinates'], []);
    const sportDistance = calculateDistance(lineString);
    const elevation = calculateElevation(lineString);
    const duration = endTime.diff(startTime, 'hour', true);
    const { maxSpeed, minSpeed } = calculateMaxMinSpeed(lineString);

    const speed = sportDistance / duration;

    let modelDifficulty;
    let modelActivity;
    if (modelExists(modelDifficultyPath)) {
      const jsonModel = JSON.parse(readFileSync(modelDifficultyPath));
      modelDifficulty = RandomForestClassifier.load(jsonModel);
    }
    if (modelExists(modelActivityPath)) {
      const jsonModel = JSON.parse(readFileSync(modelActivityPath));
      modelActivity = RandomForestClassifier.load(jsonModel);
    }

    modelActivity.train(
      [[sportDistance, duration, speed, elevation, maxSpeed, minSpeed]],
      parseInt(activityType, 10),
    );
    modelDifficulty.train(
      [[sportDistance, duration, speed, elevation, maxSpeed, minSpeed, parseInt(activityType, 10)]],
      parseInt(difficulty, 10),
    );

    writeFileSync(modelDifficultyPath, JSON.stringify(modelDifficulty.toJSON()));
    writeFileSync(modelActivityPath, JSON.stringify(modelActivity.toJSON()));

    return res.status(200).json({
      name,
      activityType,
      difficulty,
    });
  } catch (err) {
    console.log('file: createRoute.js ~ line 71 ~ module.exports= ~ err', err);
    return res.status(500).json({ err });
  }
};

/**
 * @swagger
 * /v1/route:
 *    patch:
 *      summary: Transform the gpx into a geojson and create route
 *      parameters:
 *      tags:
 *        - Route
 *      responses:
 *        "200":
 *          description: The route information has gotten successfully.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          resultMessage:
 *                              $ref: '#/components/schemas/ResultMessage'
 *                          resultCode:
 *                              $ref: '#/components/schemas/ResultCode'
 *                          user:
 *                              $ref: '#/components/schemas/User'
 *        "401":
 *          description: Invalid token.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 *        "500":
 *          description: An internal server error occurred, please try again.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Result'
 */
