const { readFileSync, readdirSync, writeFileSync, existsSync } = require('fs');
const { resolve, basename, join } = require('path');
const moment = require('moment');
const tj = require('@mapbox/togeojson');
const { default: distance } = require('@turf/distance');
const { DOMParser } = require('xmldom');
const { get } = require('lodash');
const { RandomForestClassifier } = require('../utils/ml');

const modelDificultyPath = join(__dirname, '../models/mlDificultyClassifier.json');
const modelActivityPath = join(__dirname, '../models/mlActivityClassifier.json');

const modelExists = modelPath => existsSync(modelPath);

const calculateElevation = coords =>
  coords.reduce((acc, point, i, array) => {
    try {
      const from = point[2];
      const to = array[i + 1][2];

      const elevationDifference = from - to;
      if (elevationDifference > 0) {
        return acc + elevationDifference;
      }
      return acc;
    } catch (err) {
      return acc;
    }
  }, 0);

const calculateDistance = coords =>
  coords.reduce((acc, point, i, array) => {
    try {
      const from = point;
      const to = array[i + 1];

      const currentPointDistance = distance(from, to);
      return acc + currentPointDistance;
    } catch (err) {
      return acc;
    }
  }, 0);

const calculateMaxMinSpeed = coords => {
  let minSpeed = coords[0];
  let maxSpeed = coords[0];
  coords.forEach((point, i, array) => {
    try {
      const from = point;
      const to = array[i + 1];

      const currentPointDistance = distance(from, to);
      if (minSpeed > currentPointDistance) {
        minSpeed = currentPointDistance;
      }
      if (maxSpeed < currentPointDistance) {
        maxSpeed = currentPointDistance;
      }
    } catch (err) {}
  }, 0);

  return { maxSpeed, minSpeed };
};

const mainFunction = () => {
  try {
    const options = {
      seed: 3,
      maxFeatures: 2,
      replacement: true,
      nEstimators: 200,
    };

    // Details of Matrix:
    // Type
    // Duration
    // Elevation (Diff)
    // Speed(Average)
    // Elevation

    const files = readdirSync('./seedFiles/');
    const predictionsDificulty = [];
    const predictionsActivity = [];
    let trainingDificultySet = [];
    const trainingActivitySet = files.map(file => {
      const path = resolve(join('./seedFiles/', file));
      const name = basename(file).split('_');
      const dificulty = parseInt(name[name.length - 1][0], 10);
      predictionsDificulty.push(dificulty);

      const realfile = new DOMParser().parseFromString(readFileSync(path, 'utf-8'));
      const geojson = tj.gpx(realfile);
      // Os seguintes valores são para a dificuldade:
      // Calcular Velocidade Média de todo o percurso
      // Calcular Elevação média
      // Calcular valores médio de batimento cardiaco
      // testar com variaveis nulas
      // duração do desporto
      // valores maximos, minimos e médias
      // SVM

      // Confirmar quais as variaveis mais importantes
      const { type, time } = get(geojson, 'features[0].properties', {});
      const startTime = moment(time);
      const eTime = get(geojson, ['features', 0, 'properties', 'coordTimes']);
      const endTime = moment(eTime[eTime.length - 1] || time);

      let cmdType = type;
      switch (type) {
        case 1:
        case '1':
          cmdType = 3;
          break;
        case 9:
        case '9':
          cmdType = 2;
          break;
        case 4:
        case '4':
          cmdType = 1;
          break;
        default:
          cmdType = 0;
          break;
      }
      trainingDificultySet.push(cmdType);
      predictionsActivity.push(cmdType);

      const lineString = get(geojson, ['features', 0, 'geometry', 'coordinates'], []);
      const sportDistance = calculateDistance(lineString);
      const elevation = calculateElevation(lineString);
      const duration = endTime.diff(startTime, 'hour', true);

      const speed = sportDistance / duration;
      const { maxSpeed, minSpeed } = calculateMaxMinSpeed(lineString);

      return [sportDistance, duration, speed, elevation, maxSpeed, minSpeed];
    });

    trainingDificultySet = trainingActivitySet.map((training, i) => [
      ...training,
      trainingDificultySet[i],
    ]);

    let modelDificulty;
    let modelActivity;
    if (modelExists(modelDificultyPath)) {
      const jsonModel = JSON.parse(readFileSync(modelDificultyPath));
      modelDificulty = RandomForestClassifier.load(jsonModel);
    } else {
      modelDificulty = new RandomForestClassifier(options);
    }
    if (modelExists(modelActivityPath)) {
      const jsonModel = JSON.parse(readFileSync(modelActivityPath));
      modelActivity = RandomForestClassifier.load(jsonModel);
    } else {
      modelActivity = new RandomForestClassifier(options);
    }
    modelActivity.train(trainingActivitySet, predictionsActivity);
    modelDificulty.train(trainingDificultySet, predictionsDificulty);

    const resultActivity = modelDificulty.predict(trainingActivitySet);
    const resultDificulty = modelDificulty.predict(trainingDificultySet);
    // const diferentsActivity = resultActivity.map((res, i) => res === predictionsActivity[i]);
    // const diferentsDificulty = resultDificulty.map((res, i) => res === predictionsDificulty[i]);
    writeFileSync(modelDificultyPath, JSON.stringify(modelDificulty.toJSON()));
    writeFileSync(modelActivityPath, JSON.stringify(modelActivity.toJSON()));
  } catch (error) {
    console.log('file: mlTests.js ~ line 41 ~ mainFunction ~ error', error);
  }
};

mainFunction();
