const { readFileSync, writeFileSync } = require('fs');

const tj = require('@mapbox/togeojson');
const { DOMParser } = require('xmldom');

const main = (path, path2) => {
  const realfile = new DOMParser().parseFromString(readFileSync(path, 'utf-8'));
  const geojson = tj.gpx(realfile);
  writeFileSync(path2, JSON.stringify(geojson));
};

main(
  '/home/fpronto/Mestrado/2020-2021/Semestre2/AI/TP/backend/src/seeds/transformGpx/10_04_21_ride.gpx',
  '/home/fpronto/Mestrado/2020-2021/Semestre2/AI/TP/backend/src/seeds/transformGeoJSON/10_04_21_ride.geojson',
);
