const { pool } = require('../loaders/database');

const mainFunction = async () => {
  const client = pool();
  const cmdCreateRouteTable =
    'CREATE TABLE wrr_routes ( \
    id serial PRIMARY KEY, \
    show boolean DEFAULT false,\
    name varchar (255) NOT NULL, \
    difficulty integer DEFAULT 1, \
    activityType integer DEFAULT 1, \
    distance real DEFAULT 0, \
    startDate timestamp NOT NULL, \
    endDate timestamp NOT NULL, \
    duration real DEFAULT 0, \
    geojson jsonb NOT NULL, \
    activity geometry NOT NULL\
    ); ';
  await client.connect();
  await client.query(cmdCreateRouteTable);
  await client.end();
};

mainFunction();
