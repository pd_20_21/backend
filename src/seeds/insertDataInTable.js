const { DOMParser } = require('xmldom');
const { readFileSync, readdirSync } = require('fs');
const { resolve, join, basename } = require('path');
const { get } = require('lodash');
const moment = require('moment');
const tj = require('@mapbox/togeojson');
const {
  calculations: { calculateDistance },
} = require('../utils');

const { pool } = require('../loaders/database');

const mainFunction = async () => {
  const client = pool();
  const files = readdirSync('./seedFiles/');

  const cmd =
    'INSERT INTO \
  wrr_routes (name, difficulty, activityType, distance, startDate, endDate, geojson, activity, show)\
  VALUES ';
  const finalCmd = `${files
    .reduce((acc, file) => {
      const path = resolve(join('./seedFiles/', file));
      const name = basename(file).split('_');
      const dificulty = name[name.length - 1][0];

      const realfile = new DOMParser().parseFromString(readFileSync(path, 'utf-8'));
      const geojson = tj.gpx(realfile);
      const { type, time } = get(geojson, 'features[0].properties', {});
      const startTime = moment(time);
      const eTime = get(geojson, ['features', 0, 'properties', 'coordTimes']);
      const endTime = moment(eTime[eTime.length - 1] || time);
      const lineString = get(geojson, ['features', 0, 'geometry', 'coordinates'], []);
      const distance = calculateDistance(lineString);

      let cmdType = type;
      switch (type) {
        case 1:
        case '1':
          cmdType = 3;
          break;
        case 9:
        case '9':
          cmdType = 2;
          break;
        case 4:
        case '4':
          cmdType = 1;
          break;
        default:
          cmdType = 0;
          break;
      }

      const finalAcc = `${acc} ('${get(
        geojson,
        ['features', 0, 'properties', 'name'],
        'UNKNOWN TITLE',
      ).replace(/'/g, '"')}', ${dificulty}, ${cmdType}, ${distance},'${startTime
        .toISOString()
        .replace(/T/g, ' ')}', \
    '${endTime.toISOString().replace(/T/g, ' ')}', \
    '${JSON.stringify(geojson).replace(/'/g, '"')}', '${JSON.stringify(
        geojson.features[0].geometry,
      ).replace(/'/g, '..')}', true), `;
      return finalAcc;
    }, cmd)
    .slice(0, -2)};`;

  try {
    await client.connect();
    await client.query(finalCmd);
    await client.end();
  } catch (error) {
    console.log('file: insertDataInTable.js ~ line 32 ~ mainFunction ~ error', error);
  }
};

mainFunction();
