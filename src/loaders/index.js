const expressLoader = require('./express');
const { pool } = require('./database');

module.exports = async app => {
  global.pool = pool;
  await expressLoader(app);
};
