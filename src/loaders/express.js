const express = require('express');
const cors = require('cors');
const compression = require('compression');
const morgan = require('morgan');
const helmet = require('helmet');
const api = require('../config');
const routes = require('../api/v1');

module.exports = async app => {
  process.on('uncaughtException', async error => {
    console.log('error, uncaughtException');
  });

  process.on('unhandledRejection', async ex => {
    console.log('file: express.js ~ line 17 ~ ex', ex);
    console.log('error, unhandledRejection');
  });

  app.enable('trust proxy');
  app.use(cors());
  app.use(express.json({ limit: '50mb' }));
  app.use(morgan('dev'));
  app.use(helmet());
  app.use(compression());
  app.use(express.urlencoded({ extended: false, limit: '50mb' }));

  app.use(api.prefix, routes);

  app.get('/', (_req, res) =>
    res
      .status(200)
      .json({
        resultMessage: {
          en: 'Project is successfully working...',
          pt: 'Projeto funciona corretamente...',
        },
        resultCode: '00004',
      })
      .end(),
  );

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization',
    );
    res.header('Content-Security-Policy-Report-Only', 'default-src: https:');
    if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'PUT POST PATCH DELETE GET');
      return res.status(200).json({});
    }
    return next();
  });

  app.use((_req, _res, next) => {
    const error = new Error('Endpoint could not find!');
    error.status = 404;
    next(error);
  });

  app.use((error, req, res, _next) => {
    console.log('file: express.js ~ line 63 ~ app.use ~ error', error);
    res.status(error.status || 500);
    let resultCode = '00015';
    let level = 'External Error';
    if (error.status === 500) {
      resultCode = '00013';
      level = 'Server Error';
    } else if (error.status === 404) {
      resultCode = '00014';
      level = 'Client Error';
    }

    return res.json({
      resultMessage: {
        en: error.message,
        tr: error.message,
      },
      resultCode,
    });
  });
};
