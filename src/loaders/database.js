const { Client } = require('pg');

const pool = () =>
  new Client({
    host: 'localhost',
    user: 'postgres',
    max: 20,
    database: 'TP',
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
    password: 'hovershock',
  });

module.exports = { pool };
